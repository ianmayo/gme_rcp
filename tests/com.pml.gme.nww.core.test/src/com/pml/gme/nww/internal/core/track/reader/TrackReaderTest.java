package com.pml.gme.nww.internal.core.track.reader;

import java.util.Vector;

import org.geojme.entities.VehicleTrack;
import org.junit.Test;
import org.opengis.geometry.MismatchedDimensionException;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.operation.TransformException;

import junit.framework.TestCase;

public class TrackReaderTest extends TestCase {
	
	@Test
	public void testLoad() throws MismatchedDimensionException, FactoryException, TransformException{
		
		Vector<VehicleTrack> vehicleTracks = TrackReader.load("C:/temp/boat1.rep");
		assertNotNull(vehicleTracks);
		assertEquals(1, vehicleTracks.size());
	}

}
