package com.pml.gme.nww.core.interfaces;

public interface ITimeProvider {
	
	void addTimeChangeListener(ITimeChangeListener listener);
	
	void removeTimeChangeListener(ITimeChangeListener listener);

}
