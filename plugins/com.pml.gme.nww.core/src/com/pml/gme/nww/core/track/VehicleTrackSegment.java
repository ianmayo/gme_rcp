package com.pml.gme.nww.core.track;

import gov.nasa.worldwind.tracks.TrackPoint;
import gov.nasa.worldwind.tracks.TrackSegment;

import java.util.ArrayList;
import java.util.List;

import com.vividsolutions.jts.geom.Point;

/**
 * The class to represent the Track segment of the vehicle
 *
 */
public class VehicleTrackSegment implements TrackSegment {

	private Point[] points;
	
	private List<TrackPoint> trackPoints = new ArrayList<TrackPoint>();

	/**
	 * Constructs the instance of {@link VehicleTrackSegment}
	 * @param points - the points with which the {@link TrackSegment} needs to be created
	 */
	public VehicleTrackSegment(Point[] points) {
		this.points = points;
		createTrackPoints();
	}
	
	public VehicleTrackSegment(TrackPoint point) {
		trackPoints.add(point);
	}
	
	private void createTrackPoints() {

		for(Point point : points){
			trackPoints.add(new VehicleTrackPoint( point));
		}
	}

	@Override
	public List<TrackPoint> getPoints() {
		return trackPoints;
	}

}
