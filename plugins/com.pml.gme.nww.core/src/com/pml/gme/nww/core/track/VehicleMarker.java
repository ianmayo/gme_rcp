package com.pml.gme.nww.core.track;

import java.util.List;

import com.vividsolutions.jts.geom.Point;

import gov.nasa.worldwind.tracks.Track;
import gov.nasa.worldwind.tracks.TrackPoint;
import gov.nasa.worldwind.tracks.TrackSegment;

/**
 * 
 *The Track class to represent the marker. This track represents a vehicle state for  given time
 */
public class VehicleMarker implements Track {

	private java.util.List<TrackSegment> segments = new java.util.ArrayList<TrackSegment>();

	private TrackPoint point;
	public VehicleMarker(String name, TrackPoint point) {
		this.point = point;
		setSegments();
	}

	/**
	 * Create the segments
	 */
	private void setSegments() {

		segments.add(new VehicleTrackSegment(point));

	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public int getNumPoints() {
		return 1;
	}

	@Override
	public List<TrackSegment> getSegments() {
		return segments;
	}

}
