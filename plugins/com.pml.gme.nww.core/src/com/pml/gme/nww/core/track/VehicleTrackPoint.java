package com.pml.gme.nww.core.track;

import org.geojme.entities.VehicleState;

import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.tracks.TrackPoint;

import com.vividsolutions.jts.geom.Point;

/**
 * The TrackPoint class to represent a state of a vehicle in the track
 *
 */
public class VehicleTrackPoint implements TrackPoint{

	private Point point;

	/**
	 * Constructs a instance of {@link VehicleTrackPoint}
	 * @param point
	 */
	public VehicleTrackPoint(Point point) {
		this.point = point;
	}
	
	@Override
	public double getElevation() {
		return point.getCoordinate().z;
	}

	@Override
	public double getLatitude() {
		return point.getCoordinate().y;
	}

	@Override
	public double getLongitude() {
		return point.getCoordinate().x;
	}

	@Override
	public Position getPosition() {
		 return Position.fromDegrees(getLatitude(), getLongitude(), getElevation());
	}

	@Override
	public String getTime() {
		return new Double(((VehicleState)point.getUserData()).getTime()).toString();
	}

	@Override
	public void setElevation(double arg0) {
		point.getCoordinate().z = arg0;
	}

	@Override
	public void setLatitude(double arg0) {
		 point.getCoordinate().y = arg0;
	}

	@Override
	public void setLongitude(double arg0) {
		point.getCoordinate().x = arg0;
	}

	@Override
	public void setPosition(Position arg0) {
		 point.getCoordinate().y = arg0.getLatitude().getDegrees();
	        point.getCoordinate().x = arg0.getLongitude().getDegrees();
	        point.getCoordinate().z = arg0.getElevation();
	}

	@Override
	public void setTime(String arg0) {
	}

}
