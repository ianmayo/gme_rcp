package com.pml.gme.nww.core.track;

import gov.nasa.worldwind.tracks.Track;
import gov.nasa.worldwind.tracks.TrackPoint;
import gov.nasa.worldwind.tracks.TrackSegment;

import java.util.List;

import org.geojme.entities.VehicleTrack;

import com.vividsolutions.jts.geom.MultiPoint;
import com.vividsolutions.jts.geom.Point;

public class ExtendedVehicleTrack extends VehicleTrack implements Track {

	private String name;
	
	private java.util.List<TrackSegment> segments =
	        new java.util.ArrayList<TrackSegment>();
	private int numPoints = -1;

	private Point[] points;
	public ExtendedVehicleTrack(String name, String model, MultiPoint history, Point[] points) {
		super(name, model, history);
		this.name = name;
		this.points = points;
		setSegments();
	}

	private void setSegments() {
		
		segments.add(new VehicleTrackSegment(points));
		
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getNumPoints() {

        if (this.segments == null)
            return 0;

        if (this.numPoints >= 0)
            return this.numPoints;

        this.numPoints = 0;
        for (TrackSegment segment : this.segments)
        {
            //noinspection UNUSED_SYMBOL,UnusedDeclaration
            for (TrackPoint point : segment.getPoints())
            {
                ++this.numPoints;
            }
        }

        return this.numPoints;
    
	}

	@Override
	public List<TrackSegment> getSegments() {
		return segments;
	}

}
