package com.pml.gme.nww.core.interfaces;

/**
 * The interface for listening to track changes
 * <p>
 * This interface may be implemented by clients.
 * </p>
 *
 */
public interface ITrackChangeListener {
	
	/**
     * Notifies this listener that when a track is added to the MultiTrackProvider.
     * <p>
     * This method is called when the track is added
     * </p>
     */
	void trackAdded();
	
	/**
     * Notifies this listener that when a track is removed from the MultiTrackProvider.
     * <p>
     * This method is called when the track is added
     * </p>
     */
	void trackRemoved();

}
