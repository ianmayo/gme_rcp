package com.pml.gme.nww.core.interfaces;

import java.util.Vector;

import org.geojme.entities.VehicleTrack;

/**
 * The Interface to represent the MultiTrackProvider
 */
public interface IMultiTrackProvider {

	/**
	 * 
	 * @return
	 */
	Vector<VehicleTrack> getTracks();

	/**
	 * Register a listener for a track change.
	 * @param listener
	 */
	void addTrackChangeListener(ITrackChangeListener listener);

	/**
	 * UnRegister a listener
	 * @param listener
	 */
	void removeTrackChangeListener(ITrackChangeListener listener);

}
