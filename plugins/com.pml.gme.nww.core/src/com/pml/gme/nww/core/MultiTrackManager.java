package com.pml.gme.nww.core;


import com.pml.gme.nww.core.interfaces.IMultiTrackProvider;
import com.pml.gme.nww.internal.core.providers.MultiTrackProvider;
import com.pml.gme.nww.internal.core.track.reader.TrackReader;

/**
 * The Manager class to read the harness test data
 *
 */
public class MultiTrackManager {

	private static MultiTrackManager instance;

	/**
	 * Private Constructor
	 */
	private MultiTrackManager() {

	}

	/**
	 * 
	 * @return Returns the instance of this class
	 */
	public static MultiTrackManager getInstance() {
		if (instance == null) {
			instance = new MultiTrackManager();
		}
		return instance;
	}

	/**
	 * Get the instance of the multi tracker provider based on the harness data
	 * @param path
	 * @return
	 * @throws Exception
	 */
	public IMultiTrackProvider getMultiTrackProvider(String path)
			throws Exception {
		IMultiTrackProvider provider = new MultiTrackProvider();

		provider.getTracks().addAll(TrackReader.load(path));
		return provider;
	}

}
