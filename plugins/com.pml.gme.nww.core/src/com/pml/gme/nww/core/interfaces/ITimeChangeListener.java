package com.pml.gme.nww.core.interfaces;

import com.pml.gme.nww.core.events.TimeChangedEvent;

public interface ITimeChangeListener {
	
	void timeChanged(TimeChangedEvent event);
}
