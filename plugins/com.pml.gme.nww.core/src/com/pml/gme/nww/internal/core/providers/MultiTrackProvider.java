package com.pml.gme.nww.internal.core.providers;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.geojme.entities.VehicleTrack;

import com.pml.gme.nww.core.interfaces.IMultiTrackProvider;
import com.pml.gme.nww.core.interfaces.ITrackChangeListener;

/**
 * The Class which represents the MultiTrackProvider for a given vehicle.
 * The client can use this class for notifying the track changes
 *
 */
public class MultiTrackProvider implements IMultiTrackProvider{
	
	private Vector<VehicleTrack> tracks = new Vector<VehicleTrack>();
	
	private List<ITrackChangeListener> trackChangeListeners = new ArrayList<ITrackChangeListener>();

	@Override
	public Vector<VehicleTrack> getTracks() {
		return tracks;
	}

	@Override
	public void addTrackChangeListener(ITrackChangeListener listener) {
		trackChangeListeners.add(listener);
		
	}

	@Override
	public void removeTrackChangeListener(ITrackChangeListener listener) {
		trackChangeListeners.remove(listener);
		
	}

	
}
