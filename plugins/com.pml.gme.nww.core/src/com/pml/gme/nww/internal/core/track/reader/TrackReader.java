package com.pml.gme.nww.internal.core.track.reader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.Vector;

import org.geojme.entities.VehicleState;
import org.geojme.entities.VehicleTrack;
import org.opengis.geometry.MismatchedDimensionException;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.operation.TransformException;

import MWC.GenericData.HiResDate;
import MWC.GenericData.WorldLocation;
import MWC.Utilities.TextFormatting.DebriefFormatDateTime;

import com.pml.gme.nww.core.track.ExtendedVehicleTrack;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.MultiPoint;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;

public class TrackReader {

	/**
	 * the normal token delimiter (for comma & white-space separated fields)
	 */
	private static final String normalDelimiters = " \t\n\r\f";

	/**
	 * the quoted delimiter, for quoted track names
	 */
	static final String quoteDelimiter = "\"";

	public static Vector<VehicleTrack> load(String thePath)
			throws FactoryException, MismatchedDimensionException,
			TransformException {
		final HashMap<String, Vector<Point>> myTracks = new HashMap<String, Vector<Point>>();
		final SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd hhmmss");
		sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

		try {
			BufferedReader br = new BufferedReader(new FileReader(thePath));
			String str;
			while ((str = br.readLine()) != null) {
				parseString(str, sdf, myTracks);
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();

		}

		Vector<VehicleTrack> res = new Vector<VehicleTrack>();
		Iterator<String> names = myTracks.keySet().iterator();
		while (names.hasNext()) {
			String name = (String) names.next();
			Vector<Point> list = myTracks.get(name);
			if (list.size() > 0) {
				Point[] pointArr = list.toArray(new Point[] {});
				MultiPoint pointList = new MultiPoint(pointArr,
						new GeometryFactory());
				ExtendedVehicleTrack thisE = new ExtendedVehicleTrack(name, "U48", pointList, pointArr);
				res.add(thisE);
			}
		}

		return res;
	}

	private static void parseString(String str, DateFormat sdf,
			HashMap<String, Vector<Point>> _myPoints) throws FactoryException,
			MismatchedDimensionException, TransformException {

		// get a stream from the string
		StringTokenizer st = new StringTokenizer(str);

		// declare local variables
		WorldLocation theLoc;
		double latDeg, longDeg, latMin, longMin;
		char latHem, longHem;
		double latSec, longSec;
		double theCourse;
		double theSpeed;
		double theDepth;

		String theTrackName;
		String theSymbology;

		// dateStr = new StringBuffer(100);

		// parse the line
		// 951212 050000.000 CARPET @C 12 11 10.63 N 11 41 52.37 W 269.7 2.0 0

		// first the date

		// combine the date, a space, and the time
		String dateStr = st.nextToken() + " " + st.nextToken();

		// trouble - the track name may have been quoted, in which case we will
		// pull
		// in the remaining fields aswell
		theTrackName = checkForQuotedTrackName(st);

		theSymbology = st.nextToken(normalDelimiters);

		latDeg = Double.valueOf(st.nextToken());
		latMin = Double.valueOf(st.nextToken());
		latSec = Double.valueOf(st.nextToken());

		/**
		 * now, we may have trouble here, since there may not be a space between
		 * the hemisphere character and a 3-digit latitude value - so BE CAREFUL
		 */
		String vDiff = st.nextToken();
		if (vDiff.length() > 3) {
			// hmm, they are combined
			latHem = vDiff.charAt(0);
			String secondPart = vDiff.substring(1, vDiff.length());
			longDeg = Double.valueOf(secondPart);
		} else {
			// they are separate, so only the hem is in this one
			latHem = vDiff.charAt(0);
			longDeg = Double.valueOf(st.nextToken());
		}
		longMin = Double.valueOf(st.nextToken());
		longSec = Double.valueOf(st.nextToken());
		longHem = st.nextToken().charAt(0);

		// parse (and convert) the vessel status parameters
		theCourse = MWC.Algorithms.Conversions.Degs2Rads(Double.valueOf(
				st.nextToken()).doubleValue());
		theSpeed = MWC.Algorithms.Conversions.Kts2Yps(Double.valueOf(
				st.nextToken()).doubleValue());

		// get the depth value
		String depthStr = st.nextToken();

		// we know that the Depth str may be NaN, but Java can interpret this
		// directly
		if (depthStr.equals("NaN"))
			theDepth = Double.NaN;
		else
			theDepth = Double.valueOf(depthStr).doubleValue();

		// create the tactical data
		theLoc = new WorldLocation(latDeg, latMin, latSec, latHem, longDeg,
				longMin, longSec, longHem, theDepth);

		try {
			Date date = sdf.parse(dateStr);

			// store the vehicle state
			VehicleState vs = new VehicleState(date.getTime(), theCourse,
					theSpeed);
			Coordinate trackCoord = new Coordinate(theLoc.getLong(),
					theLoc.getLat());
			trackCoord.z = theLoc.getDepth();
			Point trackPoint = new Point(new CoordinateArraySequence(
					new Coordinate[] { trackCoord }), new GeometryFactory());

			trackPoint.setUserData(vs);

			// ok, do we have this track
			Vector<Point> list = _myPoints.get(theTrackName.trim());
			if (list == null) {
				list = new Vector<Point>();
				_myPoints.put(theTrackName.trim(), list);
			}
			list.add(trackPoint);

		} catch (NumberFormatException fe) {
			fe.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}

	/**
	 * when importing the track name, it may turn out to be quoted, in which
	 * case we consume the remaining tokens until we get another quote
	 * 
	 * @param st
	 *            the tokenised stream to read the name from
	 * @return a string containing the (possibly multi-word) track name
	 */
	static public String checkForQuotedTrackName(StringTokenizer st) {
		String theTrackName = st.nextToken();

		// so, does the track name contain a quote character?
		int quoteIndex = theTrackName.indexOf("\"");
		if (quoteIndex >= 0) {
			// aah, but, we may have just read in all of the item. just check if
			// the
			// token contains
			// both speech marks...
			int secondQuoteIndex = theTrackName.indexOf("\"", quoteIndex + 1);

			if (secondQuoteIndex >= 0) {
				// yes, we have caught both quotes
				// just trim off the quote marks
				theTrackName = theTrackName.substring(1,
						theTrackName.length() - 1);
			} else {
				// no, we just caught the first quote.
				// fish around for the second one.

				String lastPartOfName = st.nextToken(quoteDelimiter);

				// yup. the ne
				theTrackName += lastPartOfName;

				// and trim away the quote
				theTrackName = theTrackName.substring(theTrackName
						.indexOf("\"") + 1);

				// consume the trailing quote delimiter (note - we allow spaces
				// & tabs)
				lastPartOfName = st.nextToken(" \t");
			}
		}
		return theTrackName;
	}

}
