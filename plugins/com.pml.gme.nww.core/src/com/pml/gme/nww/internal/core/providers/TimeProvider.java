package com.pml.gme.nww.internal.core.providers;

import java.util.ArrayList;
import java.util.List;

import com.pml.gme.nww.core.interfaces.ITimeChangeListener;
import com.pml.gme.nww.core.interfaces.ITimeProvider;

public class TimeProvider implements ITimeProvider{

	private List<ITimeChangeListener> listeners = new ArrayList<ITimeChangeListener>();
	@Override
	public void addTimeChangeListener(ITimeChangeListener listener) {
		listeners.add(listener);
		
	}

	@Override
	public void removeTimeChangeListener(ITimeChangeListener listener) {
		listeners.remove(listener);
	}

	

}
