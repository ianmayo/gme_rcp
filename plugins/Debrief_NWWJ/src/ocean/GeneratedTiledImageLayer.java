package ocean;

import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.layers.TextureTile;
import gov.nasa.worldwind.layers.TiledImageLayer;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.ScreenCredit;
import gov.nasa.worldwind.util.LevelSet;

public class GeneratedTiledImageLayer extends TiledImageLayer {
  TileImageGenerator tileImageGenerator;

  // int highestTile = 0;

  public GeneratedTiledImageLayer(TileImageGenerator tileImageGenerator,
      int tileSize) {
    this(tileImageGenerator, 0.99, tileSize);
  }

  public GeneratedTiledImageLayer(TileImageGenerator tileImageGenerator,
      double opacity, int tileSize) {
    this(createParams(tileImageGenerator, false, tileSize), tileImageGenerator,
        opacity);
  }

  public GeneratedTiledImageLayer(TileImageGenerator tileImageGenerator,
      double opacity, boolean forceLevelZero, int tileSize) {
    this(createParams(tileImageGenerator, forceLevelZero, tileSize),
        tileImageGenerator, opacity);
  }

  GeneratedTiledImageLayer(AVList params,
      TileImageGenerator tileImageGenerator, double opacity) {
    super(new LevelSet(params));

    processParams(params);

    this.tileImageGenerator = tileImageGenerator;

    setPickEnabled(false);
    setOpacity(opacity);
  }

  void processParams(AVList params) {
    String s = params.getStringValue(AVKey.DISPLAY_NAME);
    if (s != null)
      this.setName(s);

    String[] strings = (String[]) params
        .getValue(AVKey.AVAILABLE_IMAGE_FORMATS);
    if (strings != null && strings.length > 0)
      this.setAvailableImageFormats(strings);

    s = params.getStringValue(AVKey.TEXTURE_FORMAT);
    if (s != null)
      this.setTextureFormat(s);

    Double d = (Double) params.getValue(AVKey.OPACITY);
    if (d != null)
      this.setOpacity(d);

    d = (Double) params.getValue(AVKey.MAX_ACTIVE_ALTITUDE);
    if (d != null)
      this.setMaxActiveAltitude(d);

    d = (Double) params.getValue(AVKey.MIN_ACTIVE_ALTITUDE);
    if (d != null)
      this.setMinActiveAltitude(d);

    d = (Double) params.getValue(AVKey.MAP_SCALE);
    if (d != null)
      this.setValue(AVKey.MAP_SCALE, d);

    d = (Double) params.getValue(AVKey.DETAIL_HINT);
    if (d != null)
      this.setDetailHint(d);

    Boolean b = (Boolean) params.getValue(AVKey.FORCE_LEVEL_ZERO_LOADS);
    if (b != null)
      this.setForceLevelZeroLoads(b);

    b = (Boolean) params.getValue(AVKey.RETAIN_LEVEL_ZERO_TILES);
    if (b != null)
      this.setRetainLevelZeroTiles(b);

    b = (Boolean) params.getValue(AVKey.NETWORK_RETRIEVAL_ENABLED);
    if (b != null)
      this.setNetworkRetrievalEnabled(b);

    b = (Boolean) params.getValue(AVKey.USE_MIP_MAPS);
    if (b != null)
      this.setUseMipMaps(b);

    b = (Boolean) params.getValue(AVKey.USE_TRANSPARENT_TEXTURES);
    if (b != null)
      this.setUseTransparentTextures(b);

    Object o = params.getValue(AVKey.URL_CONNECT_TIMEOUT);
    if (o != null)
      this.setValue(AVKey.URL_CONNECT_TIMEOUT, o);

    o = params.getValue(AVKey.URL_READ_TIMEOUT);
    if (o != null)
      this.setValue(AVKey.URL_READ_TIMEOUT, o);

    o = params.getValue(AVKey.RETRIEVAL_QUEUE_STALE_REQUEST_LIMIT);
    if (o != null)
      this.setValue(AVKey.RETRIEVAL_QUEUE_STALE_REQUEST_LIMIT, o);

    ScreenCredit sc = (ScreenCredit) params.getValue(AVKey.SCREEN_CREDIT);
    if (sc != null)
      this.setScreenCredit(sc);

    if (params.getValue(AVKey.TRANSPARENCY_COLORS) != null)
      this.setValue(AVKey.TRANSPARENCY_COLORS,
          params.getValue(AVKey.TRANSPARENCY_COLORS));

    this.setValue(AVKey.CONSTRUCTION_PARAMETERS, params.copy());
  }

  static AVList createParams(TileImageGenerator tileImageGenerator,
      boolean forceLevelZero, int tileSize) {
    AVList params = new AVListImpl();

    params.setValue(AVKey.TILE_WIDTH, tileSize);
    params.setValue(AVKey.TILE_HEIGHT, tileSize);
    params.setValue(AVKey.NUM_LEVELS, 10);
    // params.setValue(AVKey.NUM_EMPTY_LEVELS, 0);

    params.setValue(AVKey.RETAIN_LEVEL_ZERO_TILES, true);
    // TODO - forcing level zero loads seem to cause gl context problems
    params.setValue(AVKey.FORCE_LEVEL_ZERO_LOADS, forceLevelZero);

    // params.setValue(AVKey.LEVEL_ZERO_TILE_DELTA, LatLon.fromDegrees(36, 36));
    params.setValue(AVKey.LEVEL_ZERO_TILE_DELTA, LatLon.fromDegrees(180, 180));
    params.setValue(AVKey.SECTOR, Sector.fromDegrees(-90, 90, -180, 180));

    params.setValue(AVKey.DATASET_NAME, tileImageGenerator.getDataSourceName());
    params.setValue(AVKey.DATA_CACHE_NAME,
        "holocene/" + tileImageGenerator.getDataSourceName());
    params.setValue(AVKey.FORMAT_SUFFIX, ".ter");

    params.setValue(AVKey.USE_MIP_MAPS, false);
    params.setValue(AVKey.USE_TRANSPARENT_TEXTURES, true);

    return params;
  }

  @Override
  protected void forceTextureLoad(final TextureTile tile) {
    tileImageGenerator.createTileImage(tile);

    if (tile.getLevelNumber() != 0 || !isRetainLevelZeroTiles()) {
      addTileToCache(tile);
    }
  }

  @Override
  protected void requestTexture(DrawContext dc, TextureTile tile) {
    requestQ.add(new RequestTask(tile, this));
  }

  protected static class RequestTask implements Runnable,
      Comparable<RequestTask> {
    protected final GeneratedTiledImageLayer layer;
    protected final TextureTile tile;

    protected RequestTask(TextureTile tile, GeneratedTiledImageLayer layer) {
      this.layer = layer;
      this.tile = tile;
    }

    @Override
    public void run() {
      try {
        if (Thread.currentThread().isInterrupted())
          return; // the task was cancelled because it's a duplicate
                  // or
                  // for some other reason

        layer.forceTextureLoad(tile);
        layer.getLevels().unmarkResourceAbsent(tile);
        layer.firePropertyChange(AVKey.LAYER, null, this);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    @Override
    public int compareTo(RequestTask that) {
      if (that == null) {
        throw new RuntimeException();
      }

      return tile.getPriority() == that.tile.getPriority() ? tile
          .compareTo(that.tile)
          : tile.getPriority() < that.tile.getPriority() ? -1 : 1;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      } else if (o == null || getClass() != o.getClass()) {
        return false;
      }

      RequestTask that = (RequestTask) o;

      return !(tile != null ? !tile.equals(that.tile) : that.tile != null);
    }

    @Override
    public int hashCode() {
      return tile != null ? tile.hashCode() : 0;
    }
  }

  protected void addTileToCache(TextureTile tile) {
    TextureTile.getMemoryCache().add(tile.getTileKey(), tile);
  }

}
