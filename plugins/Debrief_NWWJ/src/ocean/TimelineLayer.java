package ocean;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.media.opengl.GL;

import gov.nasa.worldwind.geom.Matrix;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.Polyline;
import ocean.TrackReader.Informations;

public class TimelineLayer extends RenderableLayer {
	
	private class Track {
		public HashMap<String, Informations> timePositions;
	}
	
	private final List<Track> _tracks = new ArrayList<TimelineLayer.Track>();
	private final TrackReader _trackReader = new TrackReader();
	private Calendar _minDatetime = null;
	private Calendar _maxDatetime = null;
	private Calendar _currentDatetime = Calendar.getInstance();
	
	public Calendar get_minDatetime() {
		return _minDatetime;
	}

	public void set_minDatetime(Calendar _minDatetime) {
		this._minDatetime = _minDatetime;
	}

	public Calendar get_maxDatetime() {
		return _maxDatetime;
	}

	public void set_maxDatetime(Calendar _maxDatetime) {
		this._maxDatetime = _maxDatetime;
	}

	public Calendar get_currentDatetime() {
		return _currentDatetime;
	}

	public void set_currentDatetime(Calendar _currentDatetime) {
		this._currentDatetime = _currentDatetime;
	}

	private float size = 600; //Cube size
	
	
	public void addTrack(String filename) {
		Track track = new Track();
		track.timePositions = new HashMap<String, TrackReader.Informations>();
		
		List<Informations> infos = new ArrayList<TrackReader.Informations>();
		List<Polyline> p = _trackReader.constructPolylineFromFile(filename, infos);
		for(int i=0; i<p.size(); i++) {
		    this.addRenderable(p.get(i));
		}
		
		//Searching for min-max dates	
		if (_minDatetime == null && _maxDatetime == null) { //First initiation
			Calendar aDate = infos.get(0).date;	
			_minDatetime = aDate;
			_maxDatetime = aDate;			
		}
		
		for(int i=0; i<infos.size(); i++) {
			Calendar aDate = infos.get(i).date;
			if(_minDatetime.after(aDate))
				_minDatetime = aDate;
			
			if(_maxDatetime.before(aDate))
				_maxDatetime = aDate;
			
			track.timePositions.put(aDate.getTime().toString(), infos.get(i));
//			System.out.println("date: " + aDate.getTime().toString());
		}	
		
		
		//Add track to tracks list
		_tracks.add(track);
		
		System.out.println("Min: " + _minDatetime.getTime().toString());
		System.out.println("Max: " + _maxDatetime.getTime().toString());
//		System.out.println(track.timePositions.toString());
		System.out.println(filename);
		setDatetime(_minDatetime);
		
	}
	
	public void setDatetime(Calendar datetime) {
		_currentDatetime.setTime(datetime.getTime());
	}
	
	@Override
	public void doRender(DrawContext dc) {
		super.doRender(dc);
		
		//Drawing cylinder from the current date time
		GL gl = dc.getGL();

        int attrMask = GL.GL_CURRENT_BIT | GL.GL_COLOR_BUFFER_BIT;

        gl.glPushAttrib(attrMask);
//        gl.glEnable(GL.GL_LIGHTING);
//        gl.glEnable(GL.GL_DEPTH_TEST);
        
        //For each tracks drawing a shape that represent a boat at current time
        for(int i=0; i<_tracks.size(); i++) {
        	Track aTrack = _tracks.get(i);
        	Informations infos = aTrack.timePositions.get(_currentDatetime.getTime().toString());
//        	System.out.println(_currentDatetime.getTime().toString());
        	if (infos != null) {
        		gl.glMatrixMode(GL.GL_MODELVIEW);

                Matrix matrix = dc.getGlobe().computeSurfaceOrientationAtPosition(Position.fromDegrees(infos.latitude, infos.longitude, -infos.depth));
                matrix = dc.getView().getModelviewMatrix().multiply(matrix);

                double[] matrixArray = new double[16];
                matrix.toArray(matrixArray, 0, false);
                gl.glLoadMatrixd(matrixArray, 0);
                
                gl.glScaled(this.size, this.size, this.size);
                drawUnitCube(dc);
        	}
        }
        
//        gl.glDisable(GL.GL_LIGHTING);
        gl.glPopAttrib();
	}
	
	/**
     * Draw a unit cube, using the active modelview matrix to orient the shape.
     *
     * @param dc Current draw context.
     */
    protected void drawUnitCube(DrawContext dc)
    {
        // Vertices of a unit cube, centered on the origin.
        float[][] v = {{-0.5f, 0.5f, -0.5f}, {-0.5f, 0.5f, 0.5f}, {0.5f, 0.5f, 0.5f}, {0.5f, 0.5f, -0.5f},
            {-0.5f, -0.5f, 0.5f}, {0.5f, -0.5f, 0.5f}, {0.5f, -0.5f, -0.5f}, {-0.5f, -0.5f, -0.5f}};

        // Array to group vertices into faces
        int[][] faces = {{0, 1, 2, 3}, {2, 5, 6, 3}, {1, 4, 5, 2}, {0, 7, 4, 1}, {0, 7, 6, 3}, {4, 7, 6, 5}};

        // Normal vectors for each face
        float[][] n = {{0, 1, 0}, {1, 0, 0}, {0, 0, 1}, {-1, 0, 0}, {0, 0, -1}, {0, -1, 0}};

        // Note: draw the cube in OpenGL immediate mode for simplicity. Real applications should use vertex arrays
        // or vertex buffer objects to achieve better performance.
        GL gl = dc.getGL();
        gl.glBegin(GL.GL_QUADS);
        try
        {
            for (int i = 0; i < faces.length; i++)
            {
                gl.glNormal3f(n[i][0], n[i][1], n[i][2]);

                for (int j = 0; j < faces[0].length; j++)
                {
                    gl.glVertex3f(v[faces[i][j]][0], v[faces[i][j]][1], v[faces[i][j]][2]);
                }
            }
        }
        finally
        {
            gl.glEnd();
        }
    }

}
