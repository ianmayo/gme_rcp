package ocean;

import gov.nasa.worldwind.View;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Box;
import gov.nasa.worldwind.geom.Extent;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Line;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.globes.Globe;
import gov.nasa.worldwind.layers.TextureTile;
import gov.nasa.worldwind.render.DrawContext;
import ocean.ElevatedSurfaceTileRenderer;
import gov.nasa.worldwind.render.Renderable;
import gov.nasa.worldwind.util.Level;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.OGLTextRenderer;
import gov.nasa.worldwind.util.PerformanceStatistic;

import java.util.ArrayList;

import javax.media.opengl.GL;

import com.sun.opengl.util.j2d.TextRenderer;

public class ElevatedTiledImageLayer extends GeneratedTiledImageLayer {
  Globe elevationGlobe;
  ElevatedSurfaceTileRenderer elevatedSurfaceTileRenderer;

  public ElevatedTiledImageLayer(TileImageGenerator tileImageGenerator,
      int tileSize, Globe elevationGlobe) {
    this(tileImageGenerator, 0.99, tileSize, elevationGlobe);
  }

  public ElevatedTiledImageLayer(TileImageGenerator tileImageGenerator,
      double opacity, int tileSize, Globe elevationGlobe) {
    super(tileImageGenerator, opacity, tileSize);

    // this.setDrawTileBoundaries(true);
    // this.setDrawTileIDs(true);
    // this.setDrawBoundingVolumes(true);

    this.elevationGlobe = elevationGlobe;
    elevatedSurfaceTileRenderer = new ElevatedSurfaceTileRenderer(
        elevationGlobe);

    setPickEnabled(false);
  }

  public Globe getElevationGlobe() {
    return elevationGlobe;
  }

  @Override
  protected void doPreRender(DrawContext dc) {
    elevatedSurfaceTileRenderer.tessellate(dc);

    super.doPreRender(dc);
  }

  @Override
  protected boolean isTileVisible(DrawContext dc, TextureTile tile) {
    return getExtent(tile, dc).intersects(
        dc.getView().getFrustumInModelCoordinates())
        && (dc.getVisibleSector() == null || dc.getVisibleSector().intersects(
            tile.getSector()));
  }

  @Override
  protected boolean needToSplit(DrawContext dc, Sector sector, Level level) {
    Vec4[] corners = sector.computeCornerPoints(getElevationGlobe(),
        dc.getVerticalExaggeration());
    Vec4 centerPoint = sector.computeCenterPoint(getElevationGlobe(),
        dc.getVerticalExaggeration());

    // Get the eye distance for each of the sector's corners and its center.
    View view = dc.getView();

    double d1 = view.getEyePoint().distanceTo3(corners[0]);
    double d2 = view.getEyePoint().distanceTo3(corners[1]);
    double d3 = view.getEyePoint().distanceTo3(corners[2]);
    double d4 = view.getEyePoint().distanceTo3(corners[3]);
    double d5 = view.getEyePoint().distanceTo3(centerPoint);

    // Find the minimum eye distance. Compute cell height at the corresponding
    // point.
    double minDistance = d1;
    double cellHeight = corners[0].getLength3() * level.getTexelSize(); // globe
                                                                        // radius
                                                                        // x
                                                                        // radian
                                                                        // texel
                                                                        // size
    double texelSize = level.getTexelSize();
    if (d2 < minDistance) {
      minDistance = d2;
      cellHeight = corners[1].getLength3() * texelSize;
    }
    if (d3 < minDistance) {
      minDistance = d3;
      cellHeight = corners[2].getLength3() * texelSize;
    }
    if (d4 < minDistance) {
      minDistance = d4;
      cellHeight = corners[3].getLength3() * texelSize;
    }
    if (d5 < minDistance) {
      minDistance = d5;
      cellHeight = centerPoint.getLength3() * texelSize;
    }

    // Split when the cell height (length of a texel) becomes greater than the
    // specified fraction of the eye
    // distance. The fraction is specified as a power of 10. For example, a
    // detail factor of 3 means split when the
    // cell height becomes more than one thousandth of the eye distance. Another
    // way to say it is, use the current
    // tile if its cell height is less than the specified fraction of the eye
    // distance.
    //
    // NOTE: It's tempting to instead compare a screen pixel size to the texel
    // size, but that calculation is
    // window-size dependent and results in selecting an excessive number of
    // tiles when the window is large.
    return cellHeight > minDistance * Math.pow(10, -this.getDetailFactor());
  }

  @Override
  public Double getMinEffectiveAltitude(Double radius) {
    if (radius == null)
      radius = getElevationGlobe().getEquatorialRadius();

    // Get the cell size for the highest-resolution level.
    double texelSize = this.getLevels().getLastLevel().getTexelSize();
    double cellHeight = radius * texelSize;

    // Compute altitude associated with the cell height at which it would switch
    // if it had higher-res levels.
    return cellHeight * Math.pow(10, this.getDetailFactor());
  }

  @Override
  public Double getMaxEffectiveAltitude(Double radius) {
    if (radius == null)
      radius = getElevationGlobe().getEquatorialRadius();

    // Find first non-empty level. Compute altitude at which it comes into
    // effect.
    for (int i = 0; i < this.getLevels().getLastLevel().getLevelNumber(); i++) {
      if (this.levels.isLevelEmpty(i))
        continue;

      // Compute altitude associated with the cell height at which it would
      // switch if it had a lower-res level.
      // That cell height is twice that of the current lowest-res level.
      double texelSize = this.levels.getLevel(i).getTexelSize();
      double cellHeight = 2 * radius * texelSize;

      return cellHeight * Math.pow(10, this.getDetailFactor());
    }

    return null;
  }

  @Override
  protected void draw(DrawContext dc) {
    this.assembleTiles(dc); // Determine the tiles to draw.

    if (this.currentTiles.size() >= 1) {
      // Indicate that this layer rendered something this frame.
      this.setValue(AVKey.FRAME_TIMESTAMP, dc.getFrameTimeStamp());

      if (this.getScreenCredit() != null) {
        dc.addScreenCredit(this.getScreenCredit());
      }

      // TODO - sorting is useless here?
      // TextureTile[] sortedTiles = new TextureTile[this.currentTiles.size()];
      // sortedTiles = this.currentTiles.toArray(sortedTiles);
      // Arrays.sort(sortedTiles, levelComparer);

      GL gl = dc.getGL();

      if (this.isUseTransparentTextures() || this.getOpacity() < 1) {
        gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT | GL.GL_POLYGON_BIT
            | GL.GL_CURRENT_BIT | GL.GL_FOG_BIT);
        this.setBlendingFunction(dc);
      } else {
        gl.glPushAttrib(GL.GL_COLOR_BUFFER_BIT | GL.GL_POLYGON_BIT
            | GL.GL_FOG_BIT);
      }

      // TODO fog does not always get along well here in some situations - seems
      // very well lit even when facing away from light
      // gl.glDisable(GL.GL_FOG);
           
      gl.glPolygonMode(GL.GL_FRONT, GL.GL_FILL);

      dc.setPerFrameStatistic(PerformanceStatistic.IMAGE_TILE_COUNT,
          this.tileCountName, this.currentTiles.size());

      elevatedSurfaceTileRenderer.renderTiles(dc, this.currentTiles);

      gl.glPopAttrib();

      // Disable depth test after rendering tiles to 
      dc.getGL().glDisable(GL.GL_DEPTH_TEST);
      //dc.getGL().glDisable(GL.GL_BLEND);
      
      if (this.isDrawTileIDs())
        this.drawTileIDs(dc, this.currentTiles);

      if (this.drawBoundingVolumes)
        this.drawBoundingVolumes(dc, this.currentTiles);

      // Check texture expiration. Memory-cached textures are checked for
      // expiration only when an explicit,
      // non-zero expiry time has been set for the layer. If none has been set,
      // the expiry times of the layer's
      // individual levels are used, but only for images in the local file
      // cache, not textures in memory. This is
      // to avoid incurring the overhead of checking expiration of in-memory
      // textures, a very rarely used feature.
      if (this.getExpiryTime() > 0
          && this.getExpiryTime() <= System.currentTimeMillis())
        this.checkTextureExpiration(dc, this.currentTiles);

      this.currentTiles.clear();
    }

    this.sendRequests();
    this.requestQ.clear();
  }

  @Override
  public boolean isLayerInView(DrawContext dc) {
    if (dc == null) {
      String message = Logging.getMessage("nullValue.DrawContextIsNull");
      Logging.logger().severe(message);
      throw new IllegalStateException(message);
    }

    if (dc.getView() == null) {
      String message = Logging
          .getMessage("layers.AbstractLayer.NoViewSpecifiedInDrawingContext");
      Logging.logger().severe(message);
      throw new IllegalStateException(message);
    }

    return !(dc.getVisibleSector() != null && !this.levels.getSector()
        .intersects(dc.getVisibleSector()));
  }

  @Override
  protected Vec4 computeReferencePoint(DrawContext dc) {
    if (dc.getViewportCenterPosition() != null)
      return getElevationGlobe().computePointFromPosition(
          dc.getViewportCenterPosition());

    java.awt.geom.Rectangle2D viewport = dc.getView().getViewport();
    int x = (int) viewport.getWidth() / 2;
    for (int y = (int) (0.5 * viewport.getHeight()); y >= 0; y--) {
      Position pos = computePositionFromScreenPoint(dc.getView(), x, y);
      if (pos == null)
        continue;

      return getElevationGlobe().computePointFromPosition(pos.getLatitude(),
          pos.getLongitude(), 0d);
    }

    return null;
  }

  public Position computePositionFromScreenPoint(View view, double x, double y) {
    if (this.elevationGlobe != null) {
      Line ray = view.computeRayFromScreenPoint(x, y);
      if (ray != null)
        return this.elevationGlobe.getIntersectionPosition(ray);
    }

    return null;
  }

  @Override
  protected void drawTileIDs(DrawContext dc, ArrayList<TextureTile> tiles) {
    java.awt.Rectangle viewport = dc.getView().getViewport();
    TextRenderer textRenderer = OGLTextRenderer.getOrCreateTextRenderer(
        dc.getTextRendererCache(), java.awt.Font.decode("Arial-Plain-13"));

    dc.getGL().glDisable(GL.GL_DEPTH_TEST);
    dc.getGL().glDisable(GL.GL_BLEND);
    dc.getGL().glDisable(GL.GL_TEXTURE_2D);

    textRenderer.beginRendering(viewport.width, viewport.height);
    textRenderer.setColor(java.awt.Color.YELLOW);
    for (TextureTile tile : tiles) {
      String tileLabel = tile.getLabel();

      if (tile.getFallbackTile() != null)
        tileLabel += "/" + tile.getFallbackTile().getLabel();

      LatLon ll = tile.getSector().getCentroid();
      Vec4 pt = getElevationGlobe()
          .computePointFromPosition(
              ll.getLatitude(),
              ll.getLongitude(),
              getElevationGlobe().getElevation(ll.getLatitude(),
                  ll.getLongitude()));
      pt = dc.getView().project(pt);
      textRenderer.draw(tileLabel, (int) pt.x, (int) pt.y);
    }
    textRenderer.setColor(java.awt.Color.WHITE);
    textRenderer.endRendering();
  }

  @Override
  protected void drawBoundingVolumes(DrawContext dc,
      ArrayList<TextureTile> tiles) {
    float[] previousColor = new float[4];
    dc.getGL().glGetFloatv(GL.GL_CURRENT_COLOR, previousColor, 0);
    dc.getGL().glColor3d(0, 1, 0);

    for (TextureTile tile : tiles) {
      if (getExtent(tile, dc) instanceof Renderable)
        ((Renderable) getExtent(tile, dc)).render(dc);
    }

    Box c = Sector.computeBoundingBox(elevationGlobe,
        dc.getVerticalExaggeration(), this.levels.getSector());
    dc.getGL().glColor3d(1, 1, 0);
    c.render(dc);

    dc.getGL().glColor4fv(previousColor, 0);
  }

  public Extent getExtent(TextureTile tile, DrawContext dc) {
    if (dc == null) {
      String msg = Logging.getMessage("nullValue.DrawContextIsNull");
      Logging.logger().severe(msg);
      throw new IllegalArgumentException(msg);
    }

    return Sector.computeBoundingBox(this.elevationGlobe,
        dc.getVerticalExaggeration(), tile.getSector());
  }

}

