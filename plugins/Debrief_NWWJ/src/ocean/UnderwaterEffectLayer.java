package ocean;

import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.DrawContext;

import javax.media.opengl.GL;

public class UnderwaterEffectLayer extends RenderableLayer {
  final static float underwaterColor[] =
      { 0.35f, 0.35f, 0.75f, 0.45f };

  double startAltitude = 0;
 
  public UnderwaterEffectLayer() {
	  
  }


  @Override
  public void doRender(DrawContext dc) {
	  
    Position eyePos = dc.getView().getEyePosition();
    if (eyePos == null)
      return;
    float alt = (float) eyePos.getElevation();
       

    // Set GL fog
    GL gl = dc.getGL();
    if (alt < startAltitude) {
	    gl.glEnable(GL.GL_FOG);
	    {
	      gl.glFogi(GL.GL_FOG_MODE, GL.GL_LINEAR);
	      gl.glFogfv(GL.GL_FOG_COLOR, underwaterColor, 0);
	      gl.glFogf(GL.GL_FOG_DENSITY, 0.35f);
	      gl.glFogf(GL.GL_FOG_START, 0);    // Start FOG
	      gl.glFogf(GL.GL_FOG_END, 1);        // End of FOG
	      gl.glHint(GL.GL_FOG_HINT, GL.GL_DONT_CARE);

	      //gl.glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
	    }
	    
	    //Draw a fullscreen quad
	    gl.glMatrixMode(GL.GL_MODELVIEW);
	    gl.glPushMatrix();
	    gl.glLoadIdentity();
	    gl.glMatrixMode(GL.GL_PROJECTION);
	    gl.glPushMatrix();
	    gl.glLoadIdentity();
	    gl.glPushAttrib(GL.GL_ENABLE_BIT);

	    try
	    {
	    	gl.glEnable(GL.GL_BLEND);
	        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

	        gl.glEnable(GL.GL_ALPHA_TEST);
	        gl.glAlphaFunc(GL.GL_GREATER, 0); // only render if alpha > 0
	        
	    	gl.glColor4f(0.0f, 0.0f, 0.0f, 0.65f);
		    gl.glBegin(GL.GL_QUADS);
		    {
			    gl.glTexCoord2f(0, 0);
			    gl.glVertex3i(-1, -1, -1);
			    gl.glTexCoord2f(1, 0);
			    gl.glVertex3i(1, -1, -1);
			    gl.glTexCoord2f(1, 1);
			    gl.glVertex3i(1, 1, -1);
			    gl.glTexCoord2f(0, 1);
			    gl.glVertex3i(-1, 1, -1);
		    }
		    gl.glEnd();
		    
		    gl.glDisable(GL.GL_ALPHA_TEST);
		    gl.glDisable(GL.GL_BLEND);
	    }
	    finally
	    {
		    gl.glPopMatrix();
		    gl.glMatrixMode(GL.GL_MODELVIEW);
		    gl.glPopMatrix();
		    gl.glPopAttrib();
	    }
	    
	    gl.glDisable(GL.GL_FOG);
	    
    } else {
    	
    	gl.glDisable(GL.GL_FOG);
    }
  }

  
  public double getStartAltitude() {
    return startAltitude;
  }

  public void setStartAltitude(double startAltitude) {
    this.startAltitude = startAltitude;
  }

  
  @Override
  public String toString() {
    return this.getName();
  }

}
