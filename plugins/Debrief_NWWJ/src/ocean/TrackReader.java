package ocean;

import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.render.Polyline;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.StringTokenizer;

public class TrackReader {
	
		private static final Color _PURPLE = new Color(160, 32, 240);
		private static final Color _BROWN = new Color(165, 42, 42);
		private static final Color _LIGHTGREEN = new Color(127, 255, 0);
		private static final Color _GOLD = new Color(255, 215, 0);
		private static final int _LineWidth = 2;
		private static final int _LineStippleFactor = 5;
		private static final short _LineStipplePattern = (short)0x5555;
		
		
		public class Informations {
			public double longitude;
			public double latitude;
			public double depth;
			public Color color;
			public Calendar date;
		}
		
		public final Object readThisLine(String theLine)
		{
			Informations infos = new Informations();
			
			// get a stream from the string
			StringTokenizer st = new StringTokenizer(theLine);

			// declare local variables
//			WorldLocation theLoc;
			double latDeg, longDeg, latMin, longMin;
			char latHem, longHem;
			double latSec, longSec;
//			HiResDate theDate = null;
			double theCourse;
			double theSpeed;
			double theDepth;

			String theTrackName;
			String theSymbology;
			String ymd;
			String hms;

			// dateStr = new StringBuffer(100);

			// parse the line
			// 951212 050000.000 CARPET @C 12 11 10.63 N 11 41 52.37 W 269.7 2.0 0

			// first the date

			// combine the date, a space, and the time
//			dateStr = st.nextToken() + " " + st.nextToken();

			// and extract the date
//			theDate = DebriefFormatDateTime.parseThis(dateStr);

			// trouble - the track name may have been quoted, in which case we will pull
			// in the remaining fields aswell
//			theTrackName = checkForQuotedTrackName(st);

			// trim the track name, just in case
//			theTrackName.trim();

//			theSymbology = st.nextToken(normalDelimiters);
			
			//Pass 4 first values for data and time
			ymd = st.nextToken();
			hms = st.nextToken();
			int year = 1900 + Integer.parseInt(ymd.substring(0, 2));
			int month = Integer.parseInt(ymd.substring(2, 4)) - 1; //0 for January
			int day = Integer.parseInt(ymd.substring(4, 6));
			int hour = Integer.parseInt(hms.substring(0, 2));
			int minute = Integer.parseInt(hms.substring(2, 4));
			float seconde = Float.parseFloat(hms.substring(4, 10));
			
//			System.out.println(year + " " + month + " " + day + " " + hour + " " + minute + " " + seconde);
			infos.date = Calendar.getInstance();
			infos.date.set(year, month, day, hour, minute, (int)seconde);
//			System.out.println(infos.date.getTime().toString());
						
			st.nextToken();
			theSymbology = st.nextToken();
//			System.out.println(theSymbology.charAt(1));

			latDeg = Double.valueOf(st.nextToken());
			latMin = Double.valueOf(st.nextToken());
			latSec = Double.valueOf(st.nextToken());

			/**
			 * now, we may have trouble here, since there may not be a space between the
			 * hemisphere character and a 3-digit latitude value - so BE CAREFUL
			 */
			String vDiff = st.nextToken();
			if (vDiff.length() > 3)
			{
				// hmm, they are combined
				latHem = vDiff.charAt(0);
				String secondPart = vDiff.substring(1, vDiff.length());
				longDeg = Double.valueOf(secondPart);
			}
			else
			{
				// they are separate, so only the hem is in this one
				latHem = vDiff.charAt(0);
				longDeg = Double.valueOf(st.nextToken());
			}
			longMin = Double.valueOf(st.nextToken());
			longSec = Double.valueOf(st.nextToken());
			longHem = st.nextToken().charAt(0);


			// parse (and convert) the vessel status parameters
//			theCourse = MWC.Algorithms.Conversions.Degs2Rads(Double.valueOf(
//					st.nextToken()).doubleValue());
//			theSpeed = MWC.Algorithms.Conversions.Kts2Yps(Double
//					.valueOf(st.nextToken()).doubleValue());

			// get the depth value
			String depthStr = st.nextToken();

			// we know that the Depth str may be NaN, but Java can interpret this
			// directly
			if (depthStr.equals("NaN"))
				theDepth = Double.NaN;
			else
				theDepth = Double.valueOf(depthStr).doubleValue();

			// create the tactical data
//			theLoc = new WorldLocation(latDeg, latMin, latSec, latHem,
//					longDeg,  longMin, longSec, longHem, theDepth);
			
			switch (theSymbology.charAt(1)) {
			case 'A':
				infos.color = Color.BLUE;
				break;
			case 'B':
				infos.color = Color.GREEN;
				break;
			case 'C':
				infos.color = Color.RED;
				break;
			case 'D':
				infos.color = Color.YELLOW;
				break;
			case 'E':
				infos.color = _PURPLE;
				break;
			case 'F':
				infos.color = Color.ORANGE;
				break;
			case 'G':
				infos.color = _BROWN;
				break;
			case 'H':
				infos.color = Color.CYAN;
				break;
			case 'I':
				infos.color = _LIGHTGREEN;
				break;
			case 'J':
				infos.color = _GOLD;
				break;
			case 'K':
				infos.color = Color.PINK;
				break;
			default:
				infos.color = Color.WHITE;
				break;
			}
			
			infos.latitude = DMSToDecimal(String.valueOf(latHem), latDeg, latMin, latSec);
			infos.longitude = DMSToDecimal(String.valueOf(longHem), longDeg, longMin, longSec);
			infos.depth = theDepth;			
			

			return infos;
		}
		
		/**
        * Conversion DMS to decimal 
        *
        * Input: latitude or longitude in the DMS format ( example: N 43� 36' 15.894")
        * Return: latitude or longitude in decimal format   
        * hemisphereOUmeridien => {W,E,S,N}
        *
        */
        public double DMSToDecimal(String hemisphereOUmeridien, double degres ,double minutes, double secondes)
        {
                double LatOrLon=0;
                double signe=1.0;
 
                if((hemisphereOUmeridien.equals("W"))||(hemisphereOUmeridien.equals("S"))) {signe=-1.0;}                
                LatOrLon = signe*(Math.floor(degres) + Math.floor(minutes)/60.0 + secondes/3600.0);
 
                return(LatOrLon);               
        }
        
	        
		/**
		 * Read track file and build a polygone from that
		 * @param filename : input file name
		 * @return : Polyline with long lat read from file
		 */
		public List<Polyline> constructPolylineFromFile(String filename, List<Informations> informations) {
			List<Polyline> polylines = new ArrayList<Polyline>();
			Polyline p = new Polyline();
			 
			BufferedReader br;
			try {
				Informations infos;
				List<Position> ll = new ArrayList<Position>();
				br = new BufferedReader(new FileReader(filename));
				String line = br.readLine();
				infos = (Informations)readThisLine(line);
				informations.add(infos);
	 	
				Color lastColor = infos.color;
				p.setLineWidth(_LineWidth);
			    p.setStippleFactor(_LineStippleFactor);
			    p.setStipplePattern(_LineStipplePattern);
			    p.setColor(infos.color);
	 	
				while (line != null) {
					infos = (Informations)readThisLine(line);
					informations.add(infos);
					if(infos.color != lastColor) {
						//If different color adding last point, finish the current line then add the line to the collection
						ll.add(new Position(Angle.fromDegrees(infos.latitude), Angle.fromDegrees(infos.longitude), -infos.depth));
						p.setPositions(ll);
						polylines.add(p);
						lastColor = infos.color;
						
						//Create new polyline with new color
						p = new Polyline();
						p.setLineWidth(_LineWidth);
					    p.setStippleFactor(_LineStippleFactor);
					    p.setStipplePattern(_LineStipplePattern);
					    p.setColor(infos.color);
						ll = new ArrayList<Position>();
					}	
            
					ll.add(new Position(Angle.fromDegrees(infos.latitude), Angle.fromDegrees(infos.longitude), -infos.depth));
            
//					System.out.println(infos.latitude + " " + infos.longitude + " " + infos.depth);
					line = br.readLine();			            
				}
        
				p.setPositions(ll);
				polylines.add(p);
//			        p.setFollowTerrain(true);
        
				br.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
 
			return polylines;			
		}
}
