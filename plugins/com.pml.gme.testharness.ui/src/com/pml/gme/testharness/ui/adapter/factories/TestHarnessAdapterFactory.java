package com.pml.gme.testharness.ui.adapter.factories;

import org.eclipse.core.runtime.IAdapterFactory;

import com.pml.gme.nww.core.interfaces.IMultiTrackProvider;
import com.pml.gme.nww.core.interfaces.ITimeProvider;
import com.pml.gme.testharness.ui.views.TestHarnessView;

public class TestHarnessAdapterFactory implements IAdapterFactory {

	@SuppressWarnings("rawtypes")
	Class[] adapterList = { IMultiTrackProvider.class };

	@Override
	public Object getAdapter(Object adaptableObject,
			@SuppressWarnings("rawtypes") Class adapterType) {
		if (adaptableObject instanceof TestHarnessView
				&& adapterType == IMultiTrackProvider.class) {

			TestHarnessView view = (TestHarnessView) adaptableObject;
			return view.getMuktiTrackProvider();

		}
		if (adaptableObject instanceof TestHarnessView
				&& adapterType == ITimeProvider.class) {

			TestHarnessView view = (TestHarnessView) adaptableObject;
			return view.getTimeProvider();

		}
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Class[] getAdapterList() {
		return adapterList;
	}

}
