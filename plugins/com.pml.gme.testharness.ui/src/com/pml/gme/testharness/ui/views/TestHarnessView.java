package com.pml.gme.testharness.ui.views;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import com.pml.gme.nww.core.MultiTrackManager;
import com.pml.gme.nww.core.interfaces.IMultiTrackProvider;
import com.pml.gme.nww.core.interfaces.ITimeProvider;
import com.pml.gme.testharness.ui.Activator;
import com.pml.gme.testharness.ui.views.providers.TestHarnessViewContentProvider;
import com.pml.gme.testharness.ui.views.providers.TestHarnessViewLabelProvider;

/**
 * 
 * The class which represents the Test Harness View
 * 
 */
public class TestHarnessView extends ViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "com.pml.gme.testharness.ui.views.TestHarnessView";

	private TableViewer viewer;

	private Action loadTestData;

	private IMultiTrackProvider provider;
	
	//Will be initialized when the slider contol is created in the view
	private ITimeProvider timeProvider;

	/**
	 * The constructor.
	 */
	public TestHarnessView() {
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	public void createPartControl(Composite parent) {

		makeActions();
		
		contributeToActionBars();
		// TODO : This is just a dummy viewer. The appropriate UI wil be placed
		// here later
		viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL
				| SWT.V_SCROLL);
		viewer.setContentProvider(new TestHarnessViewContentProvider());
		viewer.setLabelProvider(new TestHarnessViewLabelProvider());

		// TODO : The set input will be decided after what should go into the
		// view
		viewer.setInput(getViewSite());

		// Create the help context id for the viewer's control
		PlatformUI.getWorkbench().getHelpSystem()
				.setHelp(viewer.getControl(), "org.gme.testharness.ui.viewer");

	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	private void makeActions() {
		loadTestData = new Action() {

			public void run() {

				try {

					FileDialog fileDialog = new FileDialog(getSite()
							.getWorkbenchWindow().getShell(), SWT.OPEN);
					String path = fileDialog.open();
					provider = MultiTrackManager.getInstance()
							.getMultiTrackProvider(path);
				} catch (Exception e) {
					Activator
							.getDefault()
							.getLog()
							.log(new Status(IStatus.ERROR, Activator.PLUGIN_ID,
									e.getMessage()));
				}
			}
		};
		loadTestData.setText("Load");
		loadTestData.setToolTipText("Load Test Data");
		loadTestData.setImageDescriptor(PlatformUI.getWorkbench()
				.getSharedImages()
				.getImageDescriptor(ISharedImages.IMG_OBJS_INFO_TSK));

	}
	
	/**
	 * Contribute the actions to the action bars
	 */
	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	/**
	 * Fill the local pull down menu
	 * @param manager
	 */
	private void fillLocalPullDown(IMenuManager manager) {
		manager.add(loadTestData);
		manager.add(new Separator());
	}
	
	
	/**
	 * Fill the local tool bar with the actions
	 * @param manager
	 */
	private void fillLocalToolBar(IToolBarManager manager) {
		manager.add(loadTestData);
	}


	public IMultiTrackProvider getMuktiTrackProvider() {
		return provider;
	}

	public ITimeProvider getTimeProvider() {
		return timeProvider;
	}

	
}