package com.pml.gme.nww.ui.listeners;

import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.layers.TrackMarkerLayer;
import gov.nasa.worldwind.render.Polyline;
import gov.nasa.worldwind.tracks.Track;
import gov.nasa.worldwind.tracks.TrackPoint;
import gov.nasa.worldwind.tracks.TrackSegment;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.eclipse.core.runtime.Platform;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPart;

import com.pml.gme.nww.core.interfaces.IMultiTrackProvider;
import com.pml.gme.nww.core.interfaces.ITimeProvider;
import com.pml.gme.nww.core.track.ExtendedVehicleTrack;
import com.pml.gme.nww.core.track.VehicleMarker;
import com.pml.gme.nww.ui.views.NWWView;

public class PartChangeListener implements IPartListener {

	/**
	 * Used for painting the layers
	 */
	private IViewPart nwwPart;
	
	/**
	 * The track provider which is used for the tracking the vehicle
	 */
	private IMultiTrackProvider trackProvider;

	/**
	 * Constructs the instance of {@link IPartListener}
	 * @param part the instance of {@link IViewPart}
	 */
	public PartChangeListener(IViewPart part) {
		this.nwwPart = part;
	}

	@Override
	public void partActivated(IWorkbenchPart part) {

		// Plot the tracks in the view by creating a layer
		Object object = Platform.getAdapterManager().getAdapter(part,
				IMultiTrackProvider.class);
		//paint the layers one if there is a new track provider
		if (object instanceof IMultiTrackProvider && !(object == trackProvider)) {
			trackProvider = (IMultiTrackProvider) object;
			
			//plot the tracks
			Layer trackLayer = getLayers(trackProvider);
			paintLayer(trackLayer);
			
			//plot the marker
			paintLayer(getMarkerLayer(trackProvider));
			

		}

		// Plot the time marker based on the initial time from the harness view
		// and also register for new time change event
		Object timeProvider = Platform.getAdapterManager().getAdapter(part,
				ITimeProvider.class);
		if (object instanceof ITimeProvider) {
			ITimeProvider timeProviderInstance = (ITimeProvider) object;
			plotTimeMarker(timeProviderInstance);

			// register for new Time event from the harness view
			timeProviderInstance.addTimeChangeListener(new TimeChangeListener(
					nwwPart));

		}
	}

	/**
	 * The method to create the marker layer
	 * @param trackProvider
	 * @return
	 */
	Layer getMarkerLayer(IMultiTrackProvider trackProvider) {

		Vector<TrackSegment> segments = new Vector<TrackSegment>();
		if (trackProvider.getTracks() instanceof Vector<?>) {
			Vector<?> tracks = trackProvider.getTracks();
			for (Object objectTrack : tracks) {
				if (objectTrack instanceof ExtendedVehicleTrack) {
					ExtendedVehicleTrack track = (ExtendedVehicleTrack) objectTrack;
					segments.addAll(track.getSegments());
				}

			}

		}

		//Initially plot the marker at the initial point
		Track marker = new VehicleMarker("Current", segments.get(0).getPoints()
				.get(0));
		List<Track> markerTracks = new ArrayList<Track>();
		markerTracks.add(marker);
		TrackMarkerLayer layer = new TrackMarkerLayer(markerTracks);
		layer.isPickEnabled();
		layer.setOverrideElevation(true);
		layer.setElevation(0);
		return layer;

	}

	/**
	 * The method to create the track layer
	 * @param trackProvider
	 * @return Returns the Track Layer
	 */
	Layer getLayers(IMultiTrackProvider trackProvider) {

		Vector<TrackSegment> segments = new Vector<TrackSegment>();
		if (trackProvider.getTracks() instanceof Vector<?>) {
			Vector<?> tracks = trackProvider.getTracks();
			for (Object objectTrack : tracks) {
				if (objectTrack instanceof ExtendedVehicleTrack) {
					ExtendedVehicleTrack track = (ExtendedVehicleTrack) objectTrack;
					segments.addAll(track.getSegments());
				}

			}

		}

		final RenderableLayer layer = new RenderableLayer();
		for (TrackSegment segment : segments) {
			Polyline trackLine = new Polyline(new TrackToPositionIterable(
					segment));
			trackLine.setFollowTerrain(true);
			trackLine.setColor(Color.RED);
			trackLine.setAntiAliasHint(Polyline.ANTIALIAS_NICEST);
			trackLine.setLineWidth(2.5);
			layer.addRenderable(trackLine);
		}
		return layer;
	}

	/**
	 * Plot the marker on the NWW view
	 * @param layer
	 */
	void paintLayer(Layer layer) {
		if (nwwPart instanceof NWWView) {
			NWWView view = (NWWView) nwwPart;
			view.getWorld().getModel().getLayers().add(layer);
		}

	}

	void plotTimeMarker(ITimeProvider timeProvider) {

	}

	@Override
	public void partBroughtToTop(IWorkbenchPart part) {

	}

	@Override
	public void partClosed(IWorkbenchPart part) {

	}

	@Override
	public void partDeactivated(IWorkbenchPart part) {

	}

	@Override
	public void partOpened(IWorkbenchPart part) {

	}

	private static class TrackToPositionIterable implements Iterable<Position> {
		private final TrackSegment t;

		public TrackToPositionIterable(TrackSegment t) {
			this.t = t;
		}

		public Iterator<Position> iterator() {
			return new Iterator<Position>() {
				Iterator<TrackPoint> trackIt = t.getPoints().iterator();

				public boolean hasNext() {
					return trackIt.hasNext();
				}

				public Position next() {
					return trackIt.next().getPosition();
				}

				public void remove() {
					throw new UnsupportedOperationException();
				}
			};
		}
	}

}
