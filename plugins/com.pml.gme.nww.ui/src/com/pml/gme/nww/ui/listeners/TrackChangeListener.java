package com.pml.gme.nww.ui.listeners;

import org.eclipse.ui.IWorkbenchPart;

import com.pml.gme.nww.core.interfaces.ITrackChangeListener;

/**
 * The Track change listener class to listen for any changes in track
 */
public class TrackChangeListener implements ITrackChangeListener {

	private IWorkbenchPart part;

	/**
	 * Constructs the instance of {@link ITrackChangeListener}
	 * @param part
	 */
	public TrackChangeListener(IWorkbenchPart part) {
		this.part = part;
	}
	
	@Override
	public void trackAdded() {
		//Refresh the view
	}

	@Override
	public void trackRemoved() {
		//Refresh the view
	}

}
