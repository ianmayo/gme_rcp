package com.pml.gme.nww.ui.listeners;

import org.eclipse.ui.IViewPart;

import com.pml.gme.nww.core.events.TimeChangedEvent;
import com.pml.gme.nww.core.interfaces.ITimeChangeListener;

public class TimeChangeListener implements ITimeChangeListener {

	private IViewPart part;

	public TimeChangeListener(IViewPart part){
		this.part = part;
	}
	
	@Override
	public void timeChanged(TimeChangedEvent event) {

	}

}
