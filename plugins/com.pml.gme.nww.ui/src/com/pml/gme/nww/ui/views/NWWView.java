package com.pml.gme.nww.ui.views;

import gov.nasa.worldwind.Model;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.awt.WorldWindowGLCanvas;
import gov.nasa.worldwind.layers.Earth.CountryBoundariesLayer;

import java.awt.BorderLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.part.ViewPart;

import com.pml.gme.nww.ui.listeners.PartChangeListener;

/**
 * This class represents the NWW View
 * 
 */

public class NWWView extends ViewPart {

	final WorldWindowGLCanvas world = new WorldWindowGLCanvas();
	
	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "com.pml.gme.nww.ui.views.NWWView";

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	public void createPartControl(Composite parent) {

		initWorldWindLayerModel();
		Composite top = new Composite(parent, SWT.EMBEDDED);
		top.setLayoutData(new GridData(GridData.FILL_BOTH));

		java.awt.Frame worldFrame = SWT_AWT.new_Frame(top);
		java.awt.Panel panel = new java.awt.Panel(new java.awt.BorderLayout());

		worldFrame.add(panel);

		panel.add(world, BorderLayout.CENTER);
		
		parent.setLayoutData(new GridData(GridData.FILL_BOTH));
		
		configureListeners();
		

	}
	
	/**
	 * configure the part listeners to listen for activation of test harness view
	 */
	private void configureListeners()
	{
		final IPartListener partListener = new PartChangeListener(this);

		IWorkbenchWindow theWindow = getSite().getWorkbenchWindow();
		theWindow.getPartService().addPartListener(partListener);
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
	}

	/**
	 * Initialize WW model with default layers
	 */
	void initWorldWindLayerModel() {
		Model m = (Model) WorldWind
				.createConfigurationComponent(AVKey.MODEL_CLASS_NAME);
		
		m.getLayers().add(new CountryBoundariesLayer());
		world.setModel(m);
	}

	public WorldWindowGLCanvas getWorld() {
		return world;
	}
	
	

}